package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type game struct {
	id    int
	hands []hand
}

type hand struct {
	sets []set
}

type set struct {
	count  int
	colour string
}

var maxScores map[string]int = map[string]int{
	"red":   12,
	"green": 13,
	"blue":  14,
}

func main() {
	file, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	games := []game{}
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		games = append(games, parseGame(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	possibleGames := filter(games, isGamePossible)

	score := 0
	for _, pg := range possibleGames {
		score += pg.id
	}

	fmt.Println(score)
}

func filter[T any](ss []T, test func(T) bool) []T {
	ret := []T{}
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

func isGamePossible(g game) bool {
	for _, h := range g.hands {
		for _, s := range h.sets {
			if maxScores[s.colour] < s.count {
				return false
			}
		}
	}

	return true
}

func parseGame(l string) game {
	s := strings.Split(l, ": ")
	id, err := strconv.Atoi(strings.Split(s[0], " ")[1])
	if err != nil {
		log.Fatal(err)
	}

	g := game{
		id:    id,
		hands: []hand{},
	}

	for _, hs := range strings.Split(s[1], "; ") {
		g.hands = append(g.hands, parseHand(hs))
	}

	return g
}

func parseHand(hs string) hand {
	h := hand{
		sets: []set{},
	}

	for _, ss := range strings.Split(hs, ", ") {
		setStrings := strings.Split(ss, " ")
		h.sets = append(h.sets, set{
			count:  parseCount(setStrings[0]),
			colour: setStrings[1],
		})
	}

	return h
}

func parseCount(s string) int {
	c, err := strconv.Atoi(s)
	if err != nil {
		log.Fatal(err)
	}
	return c
}
