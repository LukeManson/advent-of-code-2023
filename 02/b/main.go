package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type game struct {
	id    int
	hands []hand
}

type hand struct {
	sets []set
}

type set struct {
	count  int
	colour string
}

func main() {
	score := 0
	games := parseInput("input.txt")

	for _, g := range games {
		mv := map[string]int{
			"red":   0,
			"blue":  0,
			"green": 0,
		}
		for _, h := range g.hands {
			for _, s := range h.sets {
				mv[s.colour] = max(s.count, mv[s.colour])
			}
		}

		score += mv["green"] * mv["blue"] * mv["red"]
	}

	fmt.Println(score)
}

func parseInput(filePath string) []game {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	games := []game{}
	// optionally, resize scanner's capacity for lines over 64K, see next example
	for scanner.Scan() {
		games = append(games, parseGame(scanner.Text()))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return games
}

func parseGame(l string) game {
	s := strings.Split(l, ": ")
	id, err := strconv.Atoi(strings.Split(s[0], " ")[1])
	if err != nil {
		log.Fatal(err)
	}

	g := game{
		id:    id,
		hands: []hand{},
	}

	for _, hs := range strings.Split(s[1], "; ") {
		g.hands = append(g.hands, parseHand(hs))
	}

	return g
}

func parseHand(hs string) hand {
	h := hand{
		sets: []set{},
	}

	for _, ss := range strings.Split(hs, ", ") {
		setStrings := strings.Split(ss, " ")
		h.sets = append(h.sets, set{
			count:  parseCount(setStrings[0]),
			colour: setStrings[1],
		})
	}

	return h
}

func parseCount(s string) int {
	c, err := strconv.Atoi(s)
	if err != nil {
		log.Fatal(err)
	}
	return c
}
