use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    // File hosts.txt must exist in the current path
    if let Ok(lines) = read_lines("./a.txt") {
        // Consumes the iterator, returns an (Optional) String
        let mut count = 0;
        for line in lines {
            if let Ok(ip) = line {
                let last = get_last_num(ip.clone()).to_string();
                let first = get_first_num(ip).to_string();
                let str = first + &last;
                let num = str.parse::<i32>().unwrap();
                count += num;
            }
        }
        println!("{}", count);
    }
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn get_first_num(l: String) -> char {
    for c in l.chars() { 
        if c.is_digit(10) {
            return c
        }
    }
    return '0'
}

fn get_last_num(l: String) -> char {
    return get_first_num(
        l.chars().rev().collect::<String>()
    );
}
