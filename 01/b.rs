use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    // File hosts.txt must exist in the current path
    if let Ok(lines) = read_lines("./a.txt") {
        // Consumes the iterator, returns an (Optional) String
        let mut count = 0;
        for line in lines {
            if let Ok(l) = line {
                let last = get_last_num(l.clone()).to_string();
                let first = get_first_num(l.clone()).to_string();
                let str = first + &last;
                if str == "--" {
                    println!("{}", l);
                } else {
                    println!("{}", str);
                }
                let num = str.parse::<i32>().unwrap();
                count += num;
            }
        }
        println!("{}", count);
    }
}

// The output is wrapped in a Result w to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn get_first_num(l: String) -> char {
    let mut front = "".to_owned();
    let mut f = '-';
    for c in l.chars() {
        front.push_str(&c.to_string().clone());
        let s = replace_num_strings(front.clone());
        f = get_first_num_char(s);
        if f != '-' {
            return f
        }
    }

    return f
}

fn get_last_num(l: String) -> char {
    let mut back = "".to_owned();
    let mut b = '-';
    for c in l.chars().rev() {
        back = c.to_string().to_owned() + &back;
        let s = replace_num_strings(back.clone());
        b = get_last_num_char(s);
        if b != '-' {
            return b
        }
    }

    return b
}

fn replace_num_strings(mut l: String) -> String {
    l = l.replace("seventeen", "17");
    l = l.replace("thirteen", "13");
    l = l.replace("fourteen", "14");
    l = l.replace("eighteen", "18");
    l = l.replace("nineteen", "19");
    l = l.replace("fifteen", "15");
    l = l.replace("sixteen", "16");
    l = l.replace("seventy", "70");
    l = l.replace("ninety", "90");
    l = l.replace("eighty", "80");
    l = l.replace("eleven", "11");
    l = l.replace("twelve", "12");
    l = l.replace("fourty", "40");
    l = l.replace("twenty", "20");
    l = l.replace("thirty", "30");
    l = l.replace("sixty", "60");
    l = l.replace("fifty", "50");
    l = l.replace("three", "3");
    l = l.replace("seven", "7");
    l = l.replace("eight", "8");
    l = l.replace("four", "4");
    l = l.replace("five", "5");
    l = l.replace("nine", "9");
    l = l.replace("ten", "10");
    l = l.replace("one", "1");
    l = l.replace("two", "2");
    l = l.replace("six", "6");

    return l
}

fn get_first_num_char(l: String) -> char {
    for c in l.chars() { 
        if c.is_digit(10) {
            return c
        }
    }

    return '-'
}

fn get_last_num_char(l: String) -> char {
    for c in l.chars().rev() { 
        if c.is_digit(10) {
            return c
        }
    }

    return '-'
}
